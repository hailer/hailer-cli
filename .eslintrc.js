module.exports = {
    env: {
        browser: true,
        es6: true,
        node: true
    },
    parser: '@typescript-eslint/parser',
    parserOptions: {
        project: 'tsconfig.json',
        sourceType: 'module'
    },
    ignorePatterns: ['/*.*'],
    plugins: ['@typescript-eslint'],
    rules: {
        '@typescript-eslint/consistent-type-definitions': 'error',
        '@typescript-eslint/dot-notation': 'off',
        '@typescript-eslint/explicit-member-accessibility': [
            'off',
            {
                accessibility: 'explicit'
            }
        ],
        '@typescript-eslint/member-ordering': 'error',
        '@typescript-eslint/naming-convention': [
            'error',
            {
                selector: 'property',
                format: ['strictCamelCase'],
            }
        ],
        '@typescript-eslint/no-empty-function': 'off',
        '@typescript-eslint/no-empty-interface': 'error',
        '@typescript-eslint/no-inferrable-types': 'error',
        '@typescript-eslint/no-misused-new': 'error',
        '@typescript-eslint/no-non-null-assertion': 'error',
        '@typescript-eslint/no-unused-expressions': 'error',
        '@typescript-eslint/no-unused-vars': ['warn', { vars: 'all', args: 'all' }],
        '@typescript-eslint/prefer-function-type': 'error',
        '@typescript-eslint/unified-signatures': 'error',
        'arrow-body-style': 'error',
        'semi': ["error", 'always'],
        "indent": ["error", 4, { "SwitchCase": 1 }],
        'spaced-comment': ["error", 'always'],
        'constructor-super': 'error',
        curly: 'error',
        eqeqeq: ['error', 'smart'],
        // 'guard-for-in': 'error',
        'id-blacklist': 'off',
        'id-match': 'off',
        // 'import/no-deprecated': 'warn',
        'no-bitwise': 'error',
        'no-caller': 'error',
        'no-console': [
            'error',
            {
                allow: [
                    'log',
                    'dirxml',
                    'warn',
                    'error',
                    'dir',
                    'timeLog',
                    'assert',
                    'clear',
                    'count',
                    'countReset',
                    'group',
                    'groupCollapsed',
                    'groupEnd',
                    'table',
                    'Console',
                    'markTimeline',
                    'profile',
                    'profileEnd',
                    'timeline',
                    'timelineEnd',
                    'timeStamp',
                    'context'
                ]
            }
        ],
        'no-debugger': 'error',
        'no-empty': 'off',
        'no-eval': 'error',
        'no-fallthrough': 'error',
        'no-new-wrappers': 'error',
        'no-restricted-imports': ['error', 'rxjs/Rx'],
        'no-throw-literal': 'error',
        'no-undef-init': 'error',
        'no-underscore-dangle': 'off',
        'no-unused-labels': 'error',
        'no-var': 'error',
        'prefer-const': [
            'error',
            {
                destructuring: 'all'
            }
        ],
        radix: 'error'
    },
    extends: ['prettier'],
    overrides: [
        {
            files: ['*.ts'],
            parserOptions: {
                project: ['tsconfig.json', 'e2e/tsconfig.e2e.json'],
                createDefaultProgram: true
            },
            rules: {
                'no-underscore-dangle': [
                    'error',
                    {
                        allow: [
                            '_id',
                            '_theme',
                            '_metas',
                            '_tabs',
                            '_animationDone',
                            '_error',
                            '_openedSnackBarRef',
                            '_private',
                            '_element',
                            '_componentResolver',
                            '_viewContainerRef',
                            '_extraLibs',
                            '_stateChanges',
                            '_def',
                            '_resource'
                        ]
                    }
                ],
                '@typescript-eslint/dot-notation': 'off',
                'dot-notation': 'warn',
                'brace-style': ['error', '1tbs'],
                // 'prefer-arrow/prefer-arrow-functions': 'warn',
                '@typescript-eslint/prefer-for-of': 'warn',
                'object-shorthand': 'warn',
                'no-shadow': 'off',
                '@typescript-eslint/no-shadow': 'error'
            }
        },
    ]
};
