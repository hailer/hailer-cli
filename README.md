# Hailer CLI

A Command Line interface to Hailer. It can also be used to programmatically access Hailer.

## Usage as Cli

All backend Socket API commands are available in the CLI.

Tab completion is available.

The result of each command is stored in the `result` variable.

## Install

```sh
npm install -g @hailer/cli
```

Usage: 

```sh
hailer-cli --user my.email@hailer.com
```

### Example usage

Example usage:

```javascript
await api.wall2.new_post({ subject: 'This is a wall post.', text: 'Look, this is a cool wallpost made from the Cli.' })
```

```javascript
await api.wall2.remove_post(result._id)
```

## Programmatic Usage

### Login with username and password

This example creates a wall post in Hailer, and then removes it after five seconds, and exits.

Create a new nodejs project (`npm init`) or use an existing. Add `hailer-cli` as a dependency, using `npm install @hailer/cli`

```sh
mkdir my-hailer-app
cd my-hailer-app
npm init
npm install @hailer/cli
```

Create a new file `run.js` and paste the following code (and works with typescript using the import line): 

```javascript
// import { Client } from '@hailer/cli'; // Use with typescript
const { Client } = require('@hailer/cli');

const options = {
    host: 'https://api.hailer.com',
    username: 'set-this',
    password: 'set-this',
};

Client.create(options).then(async (client) => {
    // Listen to signals from Hailer
    client.on('signals', ([name, meta]) => {
        // print all signals
        console.log('Signal:', name, meta);
    });

    // Make API request to create a new feed (wall) post.
    const post = await client.request('wall2.new_post', [{ subject: 'This is a wall post.', text: 'The content of my post' }]);

    console.log('Post created:', post);
    // client.disconnect();
}).catch(error => {
    console.log('Error:', error);
    process.exit(1);
});
```

Set the `username` and `password` parameters to a valid Hailer account.

Log in to Hailer using the browser and go to the `Feed` to see the script in action.

Start the program:

```sh
node run.js
```

### Inside async function

```javascript
// import { Client } from '@hailer/cli';
const { Client } = require('@hailer/cli');

// ...

const options = {
    host: 'https://api.hailer.com',
    username: 'set-this',
    password: 'set-this',
};

const client = await Client.create(options).catch(error => { console.log('Error:', error); process.exit(1); });

// Listen to signals from Hailer
client.on('signals', ([name, meta]) => {
    // print all signals
    console.log('Signal:', name, meta);
});

const post = await client.request('wall2.new_post', [{ subject: 'This is a wall post.', text: 'The content of my post' }]);

console.log('Post created:', post);

client.disconnect();
```

## Hailer API documentation

To find out what you can do with the Hailer API, visit:

https://api.hailer.com/apidocs
