
import { createReadStream } from 'fs';
import { basename } from 'path';
import EventEmitter from 'events';
import { io, Socket } from 'socket.io-client';
import { upload } from './commands/file-upload';
import { Readable } from 'stream';

export interface ClientOptions {
    /** Username, i.e. email address used for logging in */
    username?: string,
    /** Password */
    password?: string,
    /** Defaults to api.hailer.com */
    host?: string,
}

/**
 * # Hailer Client
 * 
 * Connect to Hailer and write your own integrations, bots or scripts.
 *
 * Example usage:
 *
 *     const { Client } = require('@hailer/cli');
 *
 *     // wrap for await to work
 *     (async () => {
 *         const options = {
 *             host: 'https://api.hailer.biz', // defaults to https://api.hailer.com
 *             username: 'set-this',
 *             password: 'set-this',
 *         };
 *
 *         const client = await Client.create(options);
 *         
 *         const post = await client.request('wall2.new_post', [{ subject: 'This is a wall post.', text: 'The content of my post' }]);
 *         
 *         console.log('Post created:', post);
 *     })();
 */
export class Client extends EventEmitter {
    /** Session key from login. Same as `hlrkey`-header used for HTTP requests */
    sessionKey?: string;
    quiet = false;
    host = 'api.hailer.com';
    username?: string;
    password?: string;

    private requestId = 0;
    private callbacks: { [requestId: number]: (error: Error | null, data?: any[]) => void } = {};
    private socket?: Socket;

    private constructor() {
        super();
    }

    /** Creates Cli instance and logs in if username and password are given */
    static async create(options: ClientOptions) {
        const instance = new Client();

        instance.host = options.host || 'api.hailer.com';

        await instance.connect(instance.host);

        if (options.username && options.password) {
            // automatically connect if username and password is given
            await instance.login(options.username, options.password);
        }        

        return instance;
    }

    async request(
        /** Operator / Endpoint, eg. `login` or `v3.discussion.message.star` */
        op: string,
        /** Arguments */
        args: any[],
    ): Promise<any> {
        return new Promise((resolve, reject) => {
            const id = ++this.requestId;
            this.callbacks[id] = (err: Error | null, data?: any[]) => {
                if (err) {
                    return reject(data);
                }
                resolve(data);
            };;
            this.socket?.emit('rpc-request', { op, args, id });

        });
    };

    /** @deprecated Use async request-function instead */
    requestCallback(
        /** Operator / Endpoint, eg. `login` or `v3.discussion.message.star` */
        op: string,
        /** Arguments */
        args: any[],
        /** Callback function */
        cb: (error: Error | null, data?: any) => void,
    ) {
        const id = ++this.requestId;
        this.callbacks[id] = cb;
        this.socket?.emit('rpc-request', { op, args, id });
    }

    async resume(sessionKey: string) {
        this.sessionKey = await this.request('resume', [sessionKey]);
    }

    async login(username: string, password: string) {
        this.sessionKey = await this.request('login', [username, password]);
    }

    /** Connect and attach message listeners, and returns promise which resolves when connected */
    async connect(hostname?: string): Promise<true | void> {
        if (!hostname) {
            hostname = this.host;
        }

        const options = { transports: ['websocket'] };

        if (
            !hostname.startsWith('http://') &&
            !hostname.startsWith('https://')
        ) {
            hostname = 'https://' + hostname;
        }

        this.socket = io(hostname, options);

        this.socket.on('rpc-response', (message) => {
            const msg = message.data;

            if (msg.err) {
                msg.ack = msg.err;
            }
            if (msg.sig) {
                if (typeof msg.sig === 'string') {
                    this.emit('signals', [msg.sig, msg.meta]);
                    return;
                }
                msg.ack = msg.sig;
            }

            if (!this.callbacks[msg.ack]) {
                // ignore unknown message
                return;
            }

            this.callbacks[msg.ack](msg.err ? msg.data : null, msg.data);

            if (!msg.sig) {
                delete this.callbacks[msg.ack];
            }
        });

        this.socket.on('error', (args) => {
            this.emit('error', args); 
        });

        this.socket.io.on('reconnect', async () => {
            this.emit('reconnect');

            if (this.sessionKey) {
                await this.resume(this.sessionKey);
            }
        });

        this.socket.on('connect', () => {
            this.emit('connect');
        });

        this.socket.on('disconnect', (reason) => {
            this.emit('disconnect', reason);
        });

        return this.socket.connected || new Promise<void>((resolve, reject) => {
            setTimeout(() => {
                if (!this.socket?.connected) {
                    reject(new Error('Timeout connecting to: ' + hostname));
                }
            }, 3000);

            this.socket?.on('connect', resolve);
        });
    };

    /** Upload file as stream, requires filename given separately */
    async uploadFileStream(stream: Readable, filename: string) {
        return await upload(stream, { sessionKey: this.sessionKey, filename, url: this.host });
    }

    /** Upload local file by name */
    async uploadFileByName(file: string) {
        const readStream = createReadStream(file);
        return await upload(readStream, { sessionKey: this.sessionKey, filename: basename(file), url: this.host });
    }

    disconnect() {
        this.socket?.disconnect();
    };
}

