import { inspect as utilInspect, InspectOptions } from 'util';

export function inspect(data: any, options?: InspectOptions): string {
    return utilInspect(data, options || { colors: true, depth: 10, breakLength: 140 });
}


// https://www.30secondsofcode.org/js/s/to-iso-string-with-timezone 2022-10-24
export const toISOStringWithTimezone = (date: Date) => {
    const tzOffset = -date.getTimezoneOffset();
    const diff = tzOffset >= 0 ? '+' : '-';
    const pad = (n: number) => `${Math.floor(Math.abs(n))}`.padStart(2, '0');
    return date.getFullYear() +
        '-' + pad(date.getMonth() + 1) +
        '-' + pad(date.getDate()) +
        'T' + pad(date.getHours()) +
        ':' + pad(date.getMinutes()) +
        ':' + pad(date.getSeconds()) +
        diff + pad(tzOffset / 60) +
        ':' + pad(tzOffset % 60);
};
