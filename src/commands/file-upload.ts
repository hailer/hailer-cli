import { Readable } from 'stream';
import { request as httpsRequest } from 'https';
import { request as httpRequest } from 'http';
import { RequestOptions } from 'https';
import { URL } from 'url';

interface UploadOptions {
    sessionKey?: string;
    filename?: string;
    filesize?: number;
    url?: string;
    method?: 'POST',
    headers?: { [header: string]: string | number },
    form?: { file: string },
}

/** Upload a file using read stream, and return file id on successful upload */
export async function upload(readStream: Readable, options?: UploadOptions) {
    const filename = options?.filename || 'file.bin';
    // const filesize = options?.filesize;
    const boundary = '----LeckingBoundary12341234';

    const httpOptions: RequestOptions = {
        path: '/upload',
        method: 'POST',
        headers: {
            // ... filesize ? { 'content-length': filesize } : null,
            ['content-type']: 'multipart/form-data; boundary=' + boundary,
            hlrkey: options?.sessionKey || '',
        },
        form: {
            file: filename
        },
        ...options,
    };

    const response = await new Promise<any>((resolve, reject) => {
        const chunks: Buffer[] = [];

        const url = new URL(options?.url || 'https://api.hailer.com');

        // decide if we should use http or https
        const request = url.protocol === 'http:' ? httpRequest : httpsRequest;
        httpOptions.host = url.hostname;
        httpOptions.port = url.port;

        const req = request(httpOptions, function (res) {
            res.on('data', function (chunk) {
                chunks.push(chunk);
            });
        });

        req.on('close', () => resolve(Buffer.concat(chunks)));

        req.on('error', reject);

        req.write("--" + boundary + "\r\n");
        req.write('Content-Disposition: form-data; name="file"; filename="' + filename + '"\r\n\r\n');

        readStream.pipe(req, { end: false });

        readStream.on('end', () => {
            req.write("--" + boundary + "\r\n");
            req.write('Content-Disposition: form-data; name="button"\r\n\r\n');
            req.write('Submit\r\n');
    
            req.write("--" + boundary + "--\r\n");
            req.end();
        });
    }).catch((error) => {
        console.log('oh fail....', error);
    });

    if (!Buffer.isBuffer(response)) {
        return 'this went wrong...';
    }

    let fileId: string | null = null;

    try {
        fileId = JSON.parse(response.toString())?._id;
    } catch (error) {
        console.log('Failed getting file id from upload', response.toString());
    }

    return fileId;
}
