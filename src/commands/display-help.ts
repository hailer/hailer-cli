import { readFileSync } from 'fs';
import { marked } from 'marked';
import { resolve } from 'path';

let pkg = {} as any;

try {
    pkg = require('../../package.json');
} catch (error) {
    pkg = require('../../../package.json');
}

export const displayHelpAndExit = () => {
    console.log(marked('# Hailer CLI ' + pkg.version + '\n\n' + readFileSync(resolve(process.cwd(), 'assets/help.md')).toString()));
    process.exit(0);
};
