import { Readable } from 'stream';
import { request } from 'https';

interface UploadOptions {
    sessionKey?: string;
    filename?: string;
    filesize?: number;
    host?: string;
    port?: string;
    path?: string;
    method?: 'POST',
    headers?: { [header: string]: string | number },
    form?: { file: string },
}

export async function upload(readStream: Readable, options?: UploadOptions) {
    const filename = options?.filename || 'file.bin';
    // const filesize = options?.filesize;
    const boundary = '----LeckingBoundary12341234';

    options = {
        host: 'api.hailer.com',
        port: '443',
        path: '/upload',
        method: 'POST',
        headers: {
            // ... filesize ? { 'content-length': filesize } : null,
            'content-type': 'multipart/form-data; boundary=' + boundary,
            hlrkey: options.sessionKey,
        },
        form: {
            file: filename
        },
        ...options,
    };

    const response = await new Promise<Buffer>((resolve, reject) => {
        const chunks: Buffer[] = [];

        const req = request(options, function (res) {
            //console.log(res);
            res.on('data', function (chunk) {
                // console.log('BODY: ' + chunk);
                chunks.push(chunk);
            });
        });

        req.on('close', () => resolve(Buffer.concat(chunks)));

        req.on('error', reject);

        req.write('--' + boundary + '\r\n');
        req.write('Content-Disposition: form-data; name="file"; filename="' + filename + '"\r\n\r\n');

        readStream.pipe(req, { end: false });

        readStream.on('end', () => {
            req.write('--' + boundary + '\r\n');
            req.write('Content-Disposition: form-data; name="button"\r\n\r\n');
            req.write('Submit\r\n');

            req.write('--' + boundary + '--\r\n');
            req.end();
        });
    }).catch((error) => { console.log('oh fail....', error); });

    if (!Buffer.isBuffer(response)) {
        return 'this went wrong...';
    }

    console.log('response:', response);

    let fileId: string | null = null;

    try {
        fileId = JSON.parse(response.toString())?._id;
    } catch (error) {
        console.log('Failed getting file id from upload', response.toString());
    }

    return fileId;
}
