import { Client } from '@hailer/cli';
import { inspect } from 'util';
import { upload } from './upload';

const email = 'lreiudhnli@mailinator.com';
const password = 'asdf';

/**
 * Hailer server to connect to
 *
 * Test enironment:
 *     https://testapi.hailer.biz/
 * Staging enironment:
 *     https://api.hailer.biz/
 * Production enironment:
 *     https://api.hailer.com/
 **/
const host = 'https://testapi.hailer.biz/';

const initialTime = 1665221192400;

interface Sync {
    messages: {
        msg?: string;
        _id: string;
        /** Message sender UserId */
        uid: string;
        /** Discussion Id */
        discussion: string;
        replyMessage: any;
        replyTo: string;
        /** Timestamp of removal */
        removed?: number;
    }[]
}

export class Bot {
    static instance: Bot;

    static async create() {
        console.log('starting bot');
        Bot.instance = new Bot();

        //                   Hailer Server to connect to,  Username                     Password
        const cli = await Client.create({ host, username: email, password });


        // fetch user object
        const info = await cli.request('v2.core.init', [['user', 'processes']]);

        const userId = info?.user?._id;

        if (!userId) {
            console.log('Failed getting bot user info.');
            cli.disconnect();
        }

        const sync: Sync = await cli.request('v2.discussion.sync', [{ timestamp: initialTime }]);

        // print out v2.discussion.sync response
        console.log('sync', sync);

        // print out processes (from v2.core.init)
        console.log('processes:', inspect(info.processes, { colors: true, depth: 10 }));

        cli.disconnect();
    }
}