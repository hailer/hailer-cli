# Getting started with Hailer CLI

## Prerequisits

Install dependencies in this directory

```sh
npm ci
```

## Run

Before you run, go into the `src/bot.ts`, and make sure the user, password and host is set properly

```sh
npm run start
```
