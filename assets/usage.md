# Hailer CLI

A Command Line interface to Hailer.

* All backend Socket API commands are available in the CLI.
* Tab completion is available.

The result of each command is stored in the `result` variable.

### Example usage

```javascript
await api.wall2.new_post({ subject: 'This is a wall post.', text: 'Look, this is a cool wallpost made from the Cli.' })
```

```javascript
await api.wall2.remove_post(result._id)
```

Upload a file and create wall post with the file:

```javascript
// fileId = await upload('/Users/akaustel/Downloads/pexels-nsu-mon-4337089.jpeg');
fileId = await upload('myfile.jpg');
await api.wall2.new_post({ subject: 'This is a wall post.', text: 'Look, this is a cool wallpost made from the Cli.', files: [fileId] })
```

