Hailer CLI can be used to control [Hailer](https://app.hailer.com) programmatically. There are currently 2 ways of use.

The first way is to use this as a pure CLI, simply by typing

```sh
hailer-cli --user user@example.com --password [the-password]
```

This will drop you into an interactive console where you can type hailer api commands directly.

The other way is to use Hailer CLI is by using programmatically, this way you can implement hailer into your own node packages and create deep integrations with Hailer.

To learn more about how to use Hailer CLI programmatically type `hailer-cli --help programmatic`.

## Notable features:

 - All backend Socket API commands are available in the CLI 
 - Tab completion is available.
 - The result of each command is stored in the `result` variable.

## Available Arguments:

*--help, -h*: displays this message

*--version, -v*: Displays versions of Hailer CLI

*--user*: Your email address used to login into hailer

*--password*: Your password used to login into hailer, or leave empty for prompt

*--host*: Which hailer backend server to use, defaults to *api.hailer.com*

## Environment variables:

*HOST*: which Hailer backend to use, defaults to *api.hailer.com*

### Example usage

```javascript
await api.wall2.new_post({ subject: 'This is a wall post.', text: 'Look, this is a cool wallpost made from the Cli.' })
```

```javascript
await api.wall2.remove_post(result._id)
```

Upload a file and create wall post with the file:

```javascript
// fileId = await upload('/Users/akaustel/Downloads/pexels-nsu-mon-4337089.jpeg');
fileId = await upload('myfile.jpg');
await api.wall2.new_post({ subject: 'This is a wall post.', text: 'Look, this is a cool wallpost made from the Cli.', files: [fileId] })
```

