import { Client } from './src/client';
import minimist from 'minimist';
import TerminalRenderer from 'marked-terminal';
import { marked } from 'marked';
import { readFileSync } from 'fs';
import { resolve } from 'path';
import colors from 'colors';
import { REPLServer, start } from 'repl';
import { displayHelpAndExit } from './src/commands/display-help';
import prompt from 'minimal-password-prompt';
import { inspect, toISOStringWithTimezone } from './src/util';

interface RootObject {
    api: any;
    /** Result from last command */
    result: any;
    /** All results as array, newest at [0] */
    history: any[];
    /** Last error */
    error: any,
};

marked.setOptions({
    // Define custom renderer
    renderer: new TerminalRenderer({
        width: 100, // only applicable when reflow is true
        reflowText: true,
    }),
});

(async () => {
    let username: string | undefined;
    let password: string | undefined;
    let host = 'https://api.hailer.com';
    
    const parsedArgs = minimist(process.argv.slice(2));

    // Displaying help if it's called at any time
    if (parsedArgs.help || parsedArgs.h) {
        displayHelpAndExit();
    }

    // Command-line arguments overwrite environment variables
    if (typeof parsedArgs.user === 'string') {
        username = parsedArgs.user;
    }

    // Command-line arguments overwrite environment variables
    if (typeof parsedArgs.password === 'string') {
        password = parsedArgs.password;
    }

    if (typeof parsedArgs.host === 'string' || process.env.HOST) {
        host = parsedArgs.host || process.env.HOST;
    }

    const quiet = !!parsedArgs.quiet;

    if (username && !password) {
        if (!quiet) {
            console.log(colors.green(username), 'logging in to', colors.yellow(host));
        }

        password = await prompt('enter password: ');
    }

    const client = await Client.create({ username, password, host }).catch(error => { console.log('Failed logging in:', error); process.exit(7); });
    let repl: REPLServer;
    let methods: any = {};

    const _methods = await client.request('methods', []);

    const root: RootObject = {
        api: {},
        /** Result from last command */
        result: undefined,
        /** All results as array, newest at [0] */
        history: [],
        /** Last error */
        error: undefined,
    };

    for (const i in _methods) {
        const path = i.split('.');
        let node = root.api;

        while (path.length > 1) {
            if (!node[path[0]]) {
                node[path[0]] = {};
            }
            node = node[path[0]];
            path.shift();
        }

        node[path[0]] = ((j) => {
            return async (...args: any[]) => {
                try {
                    root.result = await client.request(j, args);
                    root.history.unshift(root.result);

                    // manually patch the repl context for result to work properly
                    repl.context.result = root.result;
                } catch (error) {
                    root.error = error;
                    throw error;
                }

                return root.result;
            };
        })(i);

        // Init help hints
        Object.defineProperty(node[path[0]], "cmd", { value: i });
        Object.defineProperty(node[path[0]], "doc", { value: _methods[i].doc });
        Object.defineProperty(node[path[0]], "args", { value: _methods[i].args });
    };

    methods = root;

    /** Write to console without messing up the user input */
    const log = (...args: any[]) => {
        // clear line
        process.stdout.write('\x1B[2K\r');

        const timeHHMM = colors.blue('[' + toISOStringWithTimezone(new Date()).slice(11, 16) + ']');

        console.log(
            timeHHMM,
            ...args
                .map(arg => typeof arg !== 'string' && arg !== undefined ? inspect(arg) : arg)
                .filter(arg => arg !== undefined)
        );

        repl?.displayPrompt(true);
    };

    async function startRepl() {
        const repl = start({
            prompt: 'hailer> ',
            input: process.stdin,
            output: process.stdout,
            terminal: true,
            ignoreUndefined: true,
            writer: (obj) => {
                if (obj?.args) {
                    console.log(colors.yellow(obj.cmd) + '(' + colors.blue(obj.args || '') + ')', colors.white(obj.doc || ''));
                    return '';
                }

                return inspect(obj, { depth: 20, colors: true });
            }
        });

        repl.on('exit', function () {
            console.log("Bye!");
            process.exit(0);
        });

        if (client.sessionKey) {
            const data = await client.request('v2.core.init', []);
            log('Logged in successfully.');
            repl.displayPrompt(true);
            Object.assign(repl.context, data);
            repl.context.upload = client.uploadFileByName.bind(client);
        }

        for (const i in methods) {
            repl.context[i] = methods[i];
        }

        repl.context.cli = client;

        repl.context.help = () => {
            console.log(marked(readFileSync(resolve(process.cwd(), 'assets/usage.md')).toString()));

            function enumerate(node: any, depth: number) {
                for (const i in node) {
                    if (typeof node[i] === 'object') {
                        console.log('  '.repeat(depth) + colors.yellow(i));
                        enumerate(node[i], depth + 1);
                        continue;
                    }

                    console.log('  '.repeat(depth) + colors.yellow(i) + '(' + colors.blue(node[i]?.args || '') + ')', colors.white(node[i]?.doc || ''));
                }
            }

            enumerate(methods.api, 1);
        };

        return repl;
    }

    const event = (event: string, meta?: any) => {
        log(colors.green('๏'), colors.white(event), meta ? colors.gray(meta) : undefined);
    }

    client.on('disconnect', (reason) => {
        log('disconnected (attempt reconnect automatically)');
    });

    client.on('reconnect', (reason) => {
        log('reconnected');
    });

    client.on('signals', async ([signal, meta]) => {
        const info = repl.context;
        switch (signal) {
            case 'wall2.new_post':
                const data = await client.request('wall2.load_posts', [meta.post_id]);

                if (!data) {
                    // no permission to load post, in wrong workspace or permissions missing
                    return;
                }

                event('New wallpost (' + colors.white(info.users[data.uid].firstname + ' ' + info.users[data.uid].lastname) + '/' + colors.yellow(info.networks[data.cid].name) + '):');
                log('  ', colors.blue(data.initMessage.subject));
                log('  ', colors.green(data.initMessage.text));
                break;
            case 'wall2.delete_post':
                event('Removed wallpost:', meta.post_id);
                break;
            case 'wall2.new_comment':
                event('New comment.', meta.post_id);
                break;
            case 'wall2.delete_comment':
                event('Removed comment.', meta.post_id);
                break;
            case 'wall2.edited_comment':
                event('Edited comment.', meta.post_id);
                break;
            case 'activities.created':
                event('New activity', info.processes?.find((process: any) => process._id === meta.processId).name);
                break;
            case 'discussion.sync':
                event('Discussion Sync');
                break;
            default:
                event(signal, meta);
                break;
        }
    });

    repl = await startRepl();
})();
